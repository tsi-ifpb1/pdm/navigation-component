# Navigation

## Info

Este tutorial foi desenvolvido como atividade da disciplina de Programação para Dispositivos Móveis, ministrada pela Profa. Valéria Maria Bezerra Cavalcanti Maciel.

Curso de **Tecnologia em Sistemas para Internet** do Instituto Federal de Educação, Ciência e Tecnologia da Paraíba - IFPB.

## Criando o Projeto

Crie um novo projeto utilizando o modelo Empty Activity do Android Studio e clique em **Next**.

![Figura 1 - Empty Model](./tutorial_images/create_project_empty_model.jpg)

Adicione um nome para o projeto e clique em **Finish**.

![Figura 2 - Name Project](./tutorial_images/create_project_name.jpg)

## Dependências

Para incluir o suporte de navegação no seu projeto, adicione as seguintes dependências ao arquivo build.gradle do seu app:

~~~kotlin
dependencies {
    def nav_version = "2.3.1"

    // Java language implementation
    implementation "androidx.navigation:navigation-fragment:$nav_version"
    implementation "androidx.navigation:navigation-ui:$nav_version"

    // Kotlin
    implementation "androidx.navigation:navigation-fragment-ktx:$nav_version"
    implementation "androidx.navigation:navigation-ui-ktx:$nav_version"

    // Feature module Support
    implementation "androidx.navigation:navigation-dynamic-features-fragment:$nav_version"

    // Testing Navigation
    androidTestImplementation "androidx.navigation:navigation-testing:$nav_version"
}
~~~

## Criando um gráfico de Navegação

Um gráfico de navegação é um arquivo de recursos que contém todos os seus destinos e ações. O gráfico representa todos os caminhos de navegação do seu app.

![Figura 3 - Navigation Graph](./tutorial_images/navigation_graph.png)

1. Destinos são as diferentes áreas de conteúdo do seu app.
2. Ações são conexões lógicas entre seus destinos que representam os caminhos que os usuários podem seguir.

Para adicionar um gráfico de navegação ao projeto, faça o seguinte:

1. Na janela "Project", clique com o botão direito do mouse no diretório res e selecione **New > Android Resource File**. A caixa de diálogo **New Resource File** é exibida.

![Figura 4 - New Resource File](./tutorial_images/new_android_resource_file.jpg)

2. Digite um nome no campo **File name**, por exemplo, "nav_graph".

3. Selecione **Navigation** na lista suspensa **Resource type** e clique em **OK**.

![Figura 5 - New Resource File](./tutorial_images/new_android_resource_file_details.jpg)

## Navigation Editor

Depois de adicionar um gráfico, o Android Studio abre o gráfico no Navigation Editor. No Navigation Editor, você pode editar visualmente os gráficos de navegação ou editar diretamente o XML subjacente.

![Figura 6 - Navigation Editor](./tutorial_images/navigation_editor.jpg)

1. **Painel Destinations**: lista seu host de navegação e todos os destinos atualmente no **Graph Editor**.

2. **Graph Editor**: contém uma representação visual do seu gráfico de navegação. Você pode alternar entre a visualização **Design** e a representação XML subjacente na visualização **Text**.

3. **Attributes**: mostra atributos para o item selecionado no momento no gráfico de navegação.

Clique na guia **Text** para ver o XML correspondente, que precisa ser semelhante ao seguinte:

~~~xml
<?xml version="1.0" encoding="utf-8"?>
<navigation xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto" android:id="@+id/nav_graph" />
~~~

`<navigation>` é o elemento raiz de um gráfico de navegação. Conforme você adiciona destinos e conecta ações ao gráfico, é possível ver os elementos `<destination>` e `<action>` correspondentes aqui como elementos filhos. Se você tiver gráficos aninhados, eles aparecerão como elementos filhos `<navigation>`.

## Adicionar um NavHost a uma Activity

Uma das partes principais do componente de navegação é o host de navegação. O host de navegação é um contêiner vazio em que os destinos são trocados enquanto o usuário navega pelo app.

Um host de navegação precisa derivar de **NavHost**. A implementação **NavHost** padrão do componente de navegação, **NavHostFragment**, gerencia a troca de destinos de fragmento.

>**Observação:** o componente de navegação foi desenvolvido para apps que têm uma Activity principal com vários destinos de fragmento. A Activity principal está associada a um gráfico de navegação e contém um NavHostFragment que é responsável pela troca de destinos conforme necessário. Em um app com vários destinos de Activity, cada Activity tem o próprio gráfico de navegação.

### Adicionar um NavHostFragment via XML

O XML de exemplo abaixo mostra um ``NavHostFragment`` como parte da Activity principal de um app.

>**res/layout/activity_main.xml**
~~~xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <!-- FRAGMENTO ADICIONADO -->

    <androidx.fragment.app.FragmentContainerView
        android:id="@+id/nav_host_fragment"
        android:name="androidx.navigation.fragment.NavHostFragment"
        android:layout_width="0dp"
        android:layout_height="0dp"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:defaultNavHost="true"
        app:navGraph="@navigation/nav_graph" />

</androidx.constraintlayout.widget.ConstraintLayout>
~~~

Observe o seguinte:

* O atributo `android:name` contém o nome da classe da implementação de NavHost.
* O atributo `app:navGraph` associa o NavHostFragment a um gráfico de navegação. O gráfico de navegação especifica todos os destinos nesse NavHostFragment para onde os usuários podem navegar.
* O atributo `app:defaultNavHost="true"` garante que o NavHostFragment intercepte o botão "Voltar" do sistema. Observe que apenas um NavHost pode ser o padrão. Se você tiver vários hosts no mesmo layout (layouts de dois painéis, por exemplo), especifique apenas um NavHost padrão.

Você também pode usar o Layout Editor para adicionar um `NavHostFragment` a uma atividade fazendo o seguinte:

1. Na lista de arquivos de projeto, clique duas vezes no arquivo XML de layout da sua atividade para abri-lo no Layout Editor.

2. No painel **Palette**, escolha a categoria **Containers** ou, como alternativa, procure por "NavHostFragment".

![Figura 7 - New NavHostFragment](./tutorial_images/new_nav_host_fragment.jpg)

3. Arraste a visualização `NavHostFragment` até sua atividade.

4. Em seguida, na caixa de diálogo **Navigation Graphs** exibida, escolha o gráfico de navegação correspondente a ser associado a esse `NavHostFragment` e clique em **OK**.

![Figura 8 - New NavHostFragment](./tutorial_images/new_nav_host_fragment_2.jpg)

## Adicionar destinos ao gráfico de navegação

Você pode criar um destino a partir de um fragmento ou uma atividade existente. Também pode usar o Navigation Editor para criar um novo destino ou criar um marcador para depois substituir posteriormente por um fragmento ou uma atividade.

Neste exemplo, vamos criar um novo destino. Para adicionar um novo destino usando o Navigation Editor, faça o seguinte:

1. No Navigation Editor, clique no ícone **New Destination**  e, em seguida, clique em **Create new destination**.

![Figura 9 - Create Destination](./tutorial_images/create_new_destination.jpg)

2. Na caixa de diálogo New Android Component exibida, crie seu fragmento. Para saber mais sobre fragmentos, consulte a documentação do fragmento.

![Figura 10 - Create Destination](./tutorial_images/create_new_destination_2.jpg)

De volta ao Navigation Editor, observe que o Android Studio adicionou este destino ao gráfico.

![Figura 11 - Create Destination](./tutorial_images/create_new_destination_3.jpg)

## Anatomia de um destino

Clique em um destino para selecioná-lo e observe os seguintes atributos no painel **Attributes**:

- O campo **Type** indica se o destino é implementado como um fragmento, uma atividade ou outra classe personalizada no código-fonte.
- O campo **Label** contém o nome do arquivo de layout XML do destino.
- O campo **ID** contém o ID do destino que é usado para referenciar o destino no código.
- O menu suspenso **Class** mostra o nome da classe associada ao destino. Você pode clicar nessa lista suspensa para alterar a classe associada para outro tipo de destino.

![Figura 12 - Fragment Properties](./tutorial_images/fragment_properties.jpg)

Clique na guia **Text** para exibir a visualização XML do seu gráfico de navegação. O XML contém os mesmos atributos ``id``, ``name``, ``label`` e ``layout`` do destino, como mostrado abaixo.

~~~xml
<fragment
    android:id="@+id/homeFragment"
    android:name="com.example.navigation.HomeFragment"
    android:label="fragment_home"
    tools:layout="@layout/fragment_home" />
~~~

## Designar uma tela como destino inicial

O destino inicial é a primeira tela que os usuários veem ao abrir seu app, e é a última tela que os usuários veem quando saem do app. O Navigation Editor usa um ícone de casa para indicar o destino inicial.

Depois de definir todos os seus destinos, você pode escolher um destino inicial fazendo o seguinte:

1. Na guia **Design**, clique no destino para destacá-lo.

2. Clique no botão **Assign start destination**. Como alternativa, você pode clicar com o botão direito no destino e clicar em **Set as Start Destination**.

![Figura 13 - Set as Start Destination](./tutorial_images/define_start_destination.jpg)

## Conectar destinos

Uma ação é uma conexão lógica entre destinos. As ações são representadas como setas no gráfico de navegação. As ações geralmente conectam um destino ao outro, embora você também possa criar ações globais que levam a um destino específico a partir de qualquer lugar no app.

Com as ações, você representa os diferentes caminhos que os usuários podem seguir no app. Para navegar até os destinos, você ainda precisará criar o código para executar a navegação.

Você pode usar o Navigation Editor para conectar dois destinos fazendo o seguinte:

1. Na guia **Design**, passe o mouse sobre o lado direito do destino a partir do qual você quer que os usuários naveguem. Um círculo aparecerá sobre o lado direito do destino, como mostrado na Figura.

![Figura 14 - Fragment Properties](./tutorial_images/define_actions.jpg)

2. Clique o cursor sobre o destino ao qual você quer que os usuários naveguem, arraste e solte. A linha resultante entre os dois destinos representa uma ação, conforme mostrado na Figura.

>Crie um novo fragmento da mesma forma que foi criado o HomeFragment

![Figura 15 - Fragment Properties](./tutorial_images/define_actions_2.jpg)

3. Clique na seta para destacar a ação. Os seguintes atributos aparecem no painel **Attributes**:

    - O campo **Type** contém "Action".
    - O campo **ID** contém o ID da ação.
    - O campo **Destination** contém o ID do fragmento ou da atividade do destino.

4. Clique na guia **Text** para alternar para a visualização XML. Agora, um elemento de ação é adicionado ao destino de origem. A ação tem um ID e um atributo de destino que contém o ID do próximo destino, como mostrado no exemplo abaixo.

~~~xml
<?xml version="1.0" encoding="utf-8"?>
<navigation
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/nav_graph"
    app:startDestination="@id/homeFragment">
    <fragment
        android:id="@+id/homeFragment"
        android:name="com.example.navigation.HomeFragment"
        android:label="fragment_home"
        tools:layout="@layout/fragment_home" >
        <action
            android:id="@+id/action_homeFragment_to_infoFragment"
            app:destination="@id/infoFragment" />
    </fragment>
    <fragment
        android:id="@+id/infoFragment"
        android:name="com.example.navigation.InfoFragment"
        android:label="fragment_info"
        tools:layout="@layout/fragment_info" />
</navigation>
~~~

No gráfico de navegação, as ações são representadas por elementos ``<action>``. No mínimo, uma ação contém o respectivo ID e o ID do destino ao qual um usuário deve ser levado.

## Configurando HomeFragment.kt e fragment_home.xml

Quando você cria o Fragmento através do Navigation Editor, ele cria algumas configurações no arquivo que podem ser utilizadas mais a frente, vamos remover algumas linhas antes de configurar a navegação.

Este é o nosso arquivo ``fragment_home.xml``
~~~xml
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".HomeFragment">

    <!-- TODO: Update blank fragment layout -->
    <TextView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:text="@string/hello_blank_fragment" />

</FrameLayout>
~~~

Vamos adicionar um botão que redirecionará para o próximo destino. Depois das alterações o arquivo deverá ficar desta forma:

~~~xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".HomeFragment"
    android:orientation="vertical">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:text="Home"
        android:textSize="30dp"
        android:layout_weight="1"
        android:gravity="center"/>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_weight="1"
        android:gravity="center">

        <Button
            android:id="@+id/btMainProxima"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Próxima página"/>

    </LinearLayout>

</LinearLayout>
~~~

![Figura 16 - Home Fragment XML](./tutorial_images/home_fragment.jpg)

Vamos alterar o arquivo ``fragment_info.xml``

Altere os atributos `android:text`, `android:textSize` e `android:gravity`.

~~~xml
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".InfoFragment">
    
    <TextView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:text="Info Fragment" 
        android:textSize="30dp"
        android:gravity="center"/>

</FrameLayout>
~~~

Ele deve ficar como na imagem a seguir:

![Figura 17 - Info Fragment XML](./tutorial_images/info_fragment.jpg)

Agora vamos configurar o arquivo ``HomeFrament.kt``

- Remova as variáveis com ARG_PARAM e deixe no arquivo apenas as funções onCreate() e onCreateView();

O arquivo deverá ficar desta forma:

~~~kotlin
package com.example.navigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class HomeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }
}
~~~

Faça o mesmo com o arquivo ``InfoFragment.kt``, ele deve ficar desta forma:

~~~kotlin
package com.example.navigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class InfoFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false)
    }
}
~~~

## Navegar até um destino

A navegação até um destino é feita usando um ``NavController``, um objeto que gerencia a navegação do app dentro de um ``NavHost``. Cada ``NavHost`` tem o próprio ``NavController`` correspondente. Você pode recuperar um ``NavController`` usando um dos seguintes métodos:

**Kotlin:**

- Fragment.findNavController()
- View.findNavController()
- Activity.findNavController(viewId: Int)

Adicione no arquivo ``HomeFragment.kt`` as linhas:

~~~kotlin
27    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
28        super.onViewCreated(view, savedInstanceState)
29
30        val button = view.findViewById<Button>(R.id.btMainProxima)
31
32        button?.setOnClickListener {
33            findNavController().navigate(R.id.infoFragment, null)
34        }
35    }
~~~

- na linha 30 estamos recuperando o botão pelo ID

- na linha 32 estamos adicionando um listener ao botão

- e na linha 33 estavmo recuperando o navController e informando para onde devemos navegar, passando dentro da função naviget() o ID do fragmento.

Agora o aplicativo deve estar funcionando corretamente

![Figura 18 - App Final Home](./tutorial_images/app_navigation_final_1.jpeg) 

![Figura 19- Info Fragment XML](./tutorial_images/app_navigation_final_2.jpeg)

Este foi apenas um passo inicial, demonstrando como configurar Destinos utilizando o Navigation Editor e adicionando Ações entre elas. 

Pode parecer complicado para o exemplo simples de navegar entre duas páginas, mas em uma aplicação em que temos vários Destinos e diversos elementos de navegação como menu inferior, menu superior, buttons, esta é uma ferramenta poderosa.

Mais informações podem ser encontras neste [link](https://developer.android.com/guide/navigation/navigation-getting-started#kotlin).

Obrigado e bons estudos.